#include <templateEMP.h>
#include <math.h>
//#include <msp430.h>

const int LDR = BIT4;

const int green = BIT1;
const int red = BIT0;
const int blue = BIT2;

int millis = 0;

int ldr_val[10] = {0,0,0,0,0,0,0,0,0,0};

int state = 0;
int state_counter = 0;

int pulse_val = 0;
int pulse_prev_val = 0;

int char_byte_rx[8] = {0,0,0,0,0,0,0,0};
int char_byte_tx[8] = {0,0,0,0,0,0,0,0};
int bit_count = 0;

char string[50];
int string_length = 0;

void main(void)
{
    initMSP();

    // Initialize and configure timer
    TACCR0 = 0;                       // Initially stop timer
    TACCTL0 |= CCIE;                  // Enable interrupt for CCR0.
    TACTL = TASSEL_2 + ID_0 + MC_1;   // Select SMCLK, Divide by 1 , Up Mode
    _enable_interrupt();
    TACCR0 = 1000;                    // Tick every 1000 us = 1ms

    // Turn ADC on; use 16 clocks as sample & hold time
    ADC10CTL0 = ADC10ON + MSC + ADC10SHT_2;

    // Enable P1.7 as AD input
    ADC10AE0 |= LDR;

    ADC10DTC1 = 0x02;

    // port 3
    P3SEL = 0;
    P3DIR |= (red | green | blue);

    while(1){

        switch(state){

        case 1:
            receive();
            break;

        case 2:
            transmit();
            break;

        default:
            detect_start();
            break;

        }
    }
}

// function to read analog input
int analog_read(int in_channel){
    ADC10CTL1 = in_channel + CONSEQ_0;

    // Start conversion
    ADC10CTL0 |= ENC + ADC10SC;

    // Wait until result is ready
    while(ADC10CTL1 & ADC10BUSY);

    // save reading
    int reading = ADC10MEM;

    // disable ADC
    ADC10CTL0 &= ~ENC;

    return reading;
}

void detect_start(void){
    int flag = 0;
    int i = 0;

    if (serialAvailable()){
        int i = 0;

        for (i=0; i<50; i++)
            string[i] = 0;

        string_length = 0;
        string[string_length] = serialRead();

        while (string[string_length] != 10){
            string_length ++;
            string[string_length] = serialRead();
        }
        serialPrint("sending ...");
        serialFlush();

        state = 2;
        bit_count = 0;

        leds_state(1);
        __delay_cycles(100000);
        leds_state(0);
        __delay_cycles(50000);
    }

    else{
        pulse_prev_val = pulse_val;

        for (i=9; i>0; i--){
            ldr_val[i] = ldr_val[i-1];
        }


        if (analog_read(INCH_4) < 500)
            pulse_val = 0;
        else
            pulse_val = 1;


        if ((pulse_val == 0) && (pulse_val != pulse_prev_val)){
            serialPrintln("receiving...");
            bit_count = 0;
            state_counter = 0;
            state = 1;
        }
        //__delay_cycles(50000);
    }
    //serialPrintInt(state);
    //serialPrintln("");
    millis = 0;
}

void receive(void){
    if (millis > 100){

        millis = 0;

        int i = 0;

        if (analog_read(INCH_4) > 500)
            char_byte_rx[bit_count] = 1;
        else
            char_byte_rx[bit_count] = 0;

        bit_count ++;

        if (state_counter < 8)
            state_counter ++;
        else {
            print_ascii();
            state = 0;
        }

    }
}

void transmit(){
    dec_to_bin(string[0]);
    if (millis < 100){
        leds_state(char_byte_tx[bit_count]);
    }
    else {
        millis = 0;
        bit_count ++;
    }

    if (bit_count > 7){
        state = 0;
    }
}

void dec_to_bin(int dec){
    int i = 0;
    while (dec != 0){
        char_byte_tx[i] = dec%2;
        dec /= 2;
        i++;
    }
}

void print_ascii(void){

    int i = 0;
    char ascii_char[2] = {0, '\0'};

    for (i = 0; i<8; i++)
        ascii_char[0] += (char_byte_rx[i]*pow(2,i));

    serialPrint(ascii_char);
}

void leds_state(int leds_state){
    switch (leds_state){
    case 1:
        P3OUT |= (red |green | blue);
        break;
    default:
        P3OUT &= ~(red |green | blue);
        break;

    }
}

//Timer ISR - Represents half-cycle
#pragma vector = TIMER0_A0_VECTOR
__interrupt void Timer_A_CCR0_ISR(void)
{
    millis++;
}
